#include<iostream>
#include<limits>
#include"precision.cpp"
using namespace std;



int main(){
  float f;
  long double ld;
  double d;
  //cout<<"fist float: "<<precision(f)<<endl;
  cout<<"\n\n***\n\n";
  cout<<"float precision: "<<precision<float>()<<endl;
  cout<<"double precision: "<<precision<double>()<<endl;
  cout<<"long double precision: "<<precision<long double>()<<endl;

  ///
  cout<<"Machine says\n";
  cout<<"double: "<<numeric_limits<double>::epsilon()<<endl;
  cout<<"long double: "<<numeric_limits<long double>::epsilon()<<endl;

}
